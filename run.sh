# Property: aws-access-key-id
# Check if the Access Key ID exists
if [[ -z "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_ACCESS_KEY_ID" ]];
then
  fail "Property aws-access-key-id or environment variable AWS_ACCESS_KEY_ID required"
fi

# Property: aws-secret-access-key
# Check if the Secret Access Key exists
if [[ -z "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_SECRET_ACCESS_KEY" ]];
then
  fail "Property aws-secret-access-key or environment variable AWS_SECRET_ACCESS_KEY required"
fi

# Store AWS details
mkdir -p $HOME/.aws/
echo "[default]" > $HOME/.aws/credentials
echo "aws_access_key_id = $WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_ACCESS_KEY_ID" >> $HOME/.aws/credentials
echo "aws_secret_access_key = $WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_SECRET_ACCESS_KEY" >> $HOME/.aws/credentials

# Property: application-name
# Ensure that a application-name has been provided
if [[ ! -n "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_APPLICATION_NAME" ]];
then
  fail "Property application-name must be defined"
fi

# Property: environment-name
# Ensure that a environment-name has been provided
if [[ ! -n "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_S3_VERSION_BUCKET" ]];
then
  fail "Property environment-name must be defined"
fi

##
debug "Checking to see if this application version already exists"

ATTEMPTS=10
for ATTEMPT in `seq 1 $ATTEMPTS`; do
  printf "Attempt #$ATTEMPT ... "

  WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_EXISTS="$(
    aws elasticbeanstalk describe-application-versions \
      --region "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_REGION" \
      --application-name "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_APPLICATION_NAME" \
      --version-label "$WERCKER_GIT_COMMIT" \
    | python \
      -c 'import sys, json; print(len(json.load(sys.stdin)["ApplicationVersions"]))'
  )"

  echo "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_EXISTS"

  case "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_EXISTS" in
    "0" )
      info "Version $WERCKER_GIT_COMMIT will be created in ElasticBeanstalk"
      break
      ;;

    "1" )
      info "Version $WERCKER_GIT_COMMIT already exists in ElasticBeanstalk"
      return 0
      ;;
  esac

  if [[ $ATTEMPT == $ATTEMPTS ]]; then
    fail "Could not determine existence after $ATTEMPTS attempts"
  else
    sleep 5
  fi
done

##
debug "Upload version to S3"

aws s3 cp \
  "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_SOURCE_FILE" \
  "s3://$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_S3_VERSION_BUCKET/$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_S3_VERSION_KEY"

##
debug "Add version to Elastic Beanstalk"

WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_EXEC="$(
  aws elasticbeanstalk create-application-version \
    --region "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_REGION" \
    --application-name "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_APPLICATION_NAME" \
    --version-label "$WERCKER_GIT_COMMIT" \
    --description "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_DESCRIPTION" \
    --source-bundle S3Bucket="$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_S3_VERSION_BUCKET",S3Key="$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_S3_VERSION_KEY" \
    --process
)"

##
debug "Check version is processed in Elastic Beanstalk"

ATTEMPTS=10
for ATTEMPT in `seq 1 $ATTEMPTS`; do
  printf "Attempt #$ATTEMPT ... "

  WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_STATUS="$(
    aws elasticbeanstalk describe-application-versions \
      --region "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_AWS_REGION" \
      --application-name "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_APPLICATION_NAME" \
      --version-label "$WERCKER_GIT_COMMIT" \
    | python \
      -c 'import sys, json; print(json.load(sys.stdin)["ApplicationVersions"][0]["Status"])'
  )"

  echo "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_STATUS"

  case "$WERCKER_AWS_ELASTICBEANSTALK_CREATE_APPLICATION_VERSION_VERSION_STATUS" in
    "PROCESSED" )
      break
      ;;

    "FAILED" )
      fail "Application version returned failed status"
      ;;
  esac

  if [[ $ATTEMPT == $ATTEMPTS ]]; then
    fail "Could not determine status after $ATTEMPTS attempts"
  else
    sleep 5
  fi
done

##
# Delete credentials file
rm $HOME/.aws/credentials
